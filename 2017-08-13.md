August 13th, 2017
=================
Valar Morghulis
===============
```
Venue : SPACE, Vellayambalam
Time  : 2:00 PM
```

## Members Present
- Aathira P
- Ashish Kurian Thomas
- Aji Ahamed
- Athul R T
- Faiz Salam Haneef
- Gokul Das B
- Madhav V
- Pranav A

## Decision about meetings every week
- Arrangement for weekly meeting made at SPACE with Director, SPACE (Aji)
- Meetings are to be positively held every weekend. Key to SPACE is to be held by an FSUG member (Aji)
- Agenda for next meeting are to be decided by the end of the current meeting (Gokul)

## Technical Session (Initial introduction)
- Install Git
- Make an account in GitLab
- Get membership in FSUG-TVM group
- Simple Introduction to Git